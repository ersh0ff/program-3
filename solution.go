package main

import "fmt"

func main() {
	number := 600851475143
	largestPrimeFactor := findLargestPrimeFactor(number)

	fmt.Printf("The largest prime factor of %d is: %d\n", number, largestPrimeFactor)
}

func findLargestPrimeFactor(number int) int {
	i := 2

	for number > 1 {
		if number%i == 0 {
			number /= i
		} else {
			i++
		}
	}

	return i
}

